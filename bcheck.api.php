<?php

/**
 * @file
 * Documentation of Bcheck hooks.
 */

/**
 * Allow modules alter items before check print.
 * Ther you can alter customer phone number and goods items.
 * Add sales, calculate gifts and other...
 */
function hook_bcheck_prepare_items_alter(&$items, &$phone, $order) {
  //...
}


/**
 * Allow other modules alter advanced params for check.
 * New params added in advanced array by key => value pairs to check.
 */
function hook_bcheck_prepare_advanced_params_alter(&$print_params, $commerce_order){
  $print_params['advanced']['payment_mode'] = 1;
}


/**
 * This hook called before check print.
 * @param array $print_params
 *   contain elemens
 *   - order_id: printed order id
 *   - items: check items
 *   - c_num: check number
 *   - phone: customer phone number
 *   - advanced: advanced params attached to check.
 *
 * @param object $order
 *   contein current commerce_order entity.
 */
function hook_bcheck_print_check($print_params, $order){
  //You can detect check printed or not this way.
  if (isset($order->data['bcheck_print']) && $order->data['bcheck_print']) {
    // check alredy printed and print check called not first time.
  } else {
    // check never printed. This is first time call.
  }

  // And you can get check by order id.
  $check = bcheck_load_check_by_external_id( $print_params['order_id']);
}
