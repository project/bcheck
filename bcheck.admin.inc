<?php
/**
 * @file
 * Administration pages callback functions.
 */

/**
 * Setings page callback form.
 */
function bcheck_settings_form() {
  $form['bcheck_appid'] = array(
    '#type' => 'textfield',
    '#title' => t('APP ID'),
    '#default_value' => variable_get('bcheck_appid'),
    '#required' => TRUE,
  );
  $form['bcheck_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#default_value' => variable_get('bcheck_secret_key'),
    '#required' => TRUE,
  );
  $form['bcheck_cashier'] = array(
    '#type' => 'textfield',
    '#title' => t('Cashier name'),
    '#default_value' => variable_get('bcheck_cashier'),
    '#required' => TRUE,
  );
  $form['phone'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#title' => t('Phone number'),
    '#tree' => FALSE,
  );
  $form['phone']['bcheck_phone_number_field'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Phone number field token'),
    '#title_display' => 'invisible',
    '#default_value' => variable_get('bcheck_phone_number_field'),
  );

  $form['phone']['token_help'] = array(
    '#theme' => 'token_tree_link',
    '#token_types' => array('commerce-order'),
  );

  return system_settings_form($form);
}

/**
 * Edit page callback form.
 */
function bcheck_edit_form($form, $form_state) {
  $query = drupal_get_query_parameters();
  $order_id = isset($query['order_id']) ? (int) $query['order_id'] : NULL;
  $form['order_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Order id'),
    '#size' => 10,
    '#default_value' => $order_id,
  );
  $form['action'] = array(
    '#type' => 'action',
  );
  $form['action']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Find'),
  );
  $form['action']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('bcheck_reset_form'),
  );
  $form['data'] = bcheck_edit_table($order_id);
  return $form;
}

function bcheck_reset_form($form, &$form_state) {
  $form_state['redirect'] = request_path();
}

function bcheck_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  $form_state['redirect'] = array(request_path(), array('query' => $values));
}


function bcheck_edit_table($order_id) {
  $checks = bchec_check_load_by_order_id($order_id);
  $header = array(
    t('Id'),
    t('Check id'),
    t('External id'),
    t('Status'),
    t('Check total'),
    t('Created date'),
    t('Action'),
  );
  $rows = array();

  $actions = array(
    '#theme' => 'links',
    '#links' => array(),
  );
  foreach ($checks as $cid => $check) {
    $actions['#links'] = array();
    $actions['#links']['view'] = array(
      'title' => t('View check details'),
      'href' => 'admin/config/services/bcheck/ajax/view/' . $check->cid . '/nojs',
      'attributes' => array(
        'class' => array(
          'ctools-use-modal',
          'ctools-modal-bcheck-view-details',
        ),
      ),
    );
    if ($check->status == BCHECK_STATUS_PRINTED) {
      $actions['#links']['refund'] = array(
        'title' => t('Print refund'),
        'href' => 'admin/config/services/bcheck/refund/' . $cid,
      );
    }
    $actions['#links']['edit'] = array(
      'title' => t('Edit order'),
      'href' => 'admin/commerce/orders/' . $check->external_id . '/edit',
    );

    $created_date = format_date($check->created, 'custom', 'd.m.Y H:i');
    $rows[$cid] = array(
      'cid' => array('data' => $check->cid),
      'c_num' => array('data' => $check->c_num),
      'external_id' => array('data' => $check->external_id),
      'status' => bcheck_get_status($check->status),
      'summ' => array('data' => $check->summ),
      'created' => array('data' => $created_date),
      'action' => render($actions),
    );
  }

  _bcheck_modal_settings();
  return array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
    '#attached' => array(
      'library' => array(array('system', 'drupal.ajax')),
    ),
  );
}

function _bcheck_modal_settings() {
  static $added = FALSE;
  if ($added == FALSE) {
    $added = TRUE;
    // Include the CTools tools that we need.
    ctools_include('modal');
    ctools_include('ajax');
    ctools_modal_add_js();


    $bcheck_modal_styles = array(
      'bcheck-view-details' => array(
        'modalSize' => array(
          'type' => 'scale',
          'width' => 0.7,
          'height' => 0.5,
        ),
        'modalOptions' => array(
          'opacity' => (float) 0.3,
          'background-color' => '#000000',
        ),
        'closeText' => '',
        'loadingText' => '',
        'animation' => 'fadeIn',
        'animationSpeed' => 'fast',
      ),
    );


    drupal_add_js($bcheck_modal_styles, 'setting');
  }
}

function bcheck_refund_confirm_form($form, $form_state, $bcheck) {
  $form['bcheck'] = array(
    '#type' => 'value',
    '#value' => $bcheck,
  );
  $return_path = array(
    'path' => 'admin/config/services/bcheck/editor',
    'query' => array('order_id' => $bcheck->external_id),
  );
  return confirm_form($form, t('Do you really want to print refund bill for order !external_id?', array('!external_id' => $bcheck->external_id)), $return_path);
}

function bcheck_refund_confirm_form_submit($form, &$form_state) {
  $bcheck = $form_state['values']['bcheck'];
  bcheck_print_refund_bill($bcheck->data, $bcheck->cid);
  $order = commerce_order_load($bcheck->external_id);
  if ($order) {
    $order->data['bcheck_print'] = FALSE;
    commerce_order_save($order);
  }
  $form_state['redirect'] = array('admin/config/services/bcheck/editor', array('order_id' => $bcheck->external_id));
}

function bcheck_view_check_details($bcheck, $full_page = TRUE) {
  $back_link = array(
    '#theme' => 'link',
    '#text' => t('Return to checks list'),
    '#path' => 'admin/config/services/bcheck/editor',
    '#options' => array(
      'html' => FALSE,
      'attributes' => array(),
      'query' => array(
        'order_id' => $bcheck->external_id,
      ),
    ),
  );
  $output = array();
  if ($full_page) {
    $output['back_first'] = $back_link;
  }
  $order_link_attributes = array(
    'attributes' => array(
      'title' => t('Edit order'),
    ),
  );
  $output['info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Check info'),
    'data' => array(
      '#theme' => 'table',
      '#header' => array(
        t('Id'),
        t('Order id'),
        t('Total summ'),
        t('Created'),
        t('Status'),
      ),
      '#rows' => array(
        array(
          $bcheck->cid,
          l($bcheck->external_id, 'admin/commerce/orders/' . $bcheck->external_id . '/edit', $order_link_attributes),
          $bcheck->summ,
          format_date($bcheck->created, 'custom', 'd.m.Y H:i:s'),
          bcheck_get_status($bcheck->status),
        ),
      ),
    ),
  );
  $output['client'] = array(
    '#type' => 'fieldset',
    '#title' => t('Client info'),
    'data' => array(
      '#theme' => 'table',
      '#header' => array(
        t('Contact (sms/email)'),
        t('Payed cashless'),
        t('Payed cash'),
        t('Cashier'),
      ),
      '#rows' => array(
        array(
          $bcheck->data['smsEmail54FZ'],
          $bcheck->data['payed_cashless'],
          $bcheck->data['payed_cash'],
          $bcheck->data['author'],
        ),
      ),
    ),
  );
  $output['goods'] = array(
    '#type' => 'fieldset',
    '#title' => t('Goods'),
    'data' => array(
      '#theme' => 'table',
      '#header' => array(
        t('Product title'),
        t('Quantity'),
        t('Price'),
        t('Total'),
      ),
      '#rows' => array(),
    ),
  );
  foreach ($bcheck->data['goods'] as $product) {
    $output['goods']['data']['#rows'][] = array(
      $product['name'],
      $product['count'],
      $product['price'],
      $product['sum'],
    );
  }

  if ($full_page) {
    $output['back_last'] = $back_link;
  }

  return $output;
}

function bcheck_view_check_details_ajax($bcheck, $js) {
  $commands = array();
  if (!$js) {
    drupal_goto('admin/config/services/bcheck/view/' . $bcheck->cid);
  }
  ctools_include('modal');
  $commands[] = ctools_modal_command_display(t('Bcheck check details'), bcheck_view_check_details($bcheck, FALSE));
  return array('#type' => 'ajax', '#commands' => $commands);
}
