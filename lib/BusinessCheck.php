<?php

class BusinessCheck {
  /**
   * Адрес для получения токена.
   */
  const GET_TOKEN_URL = "Token";
  /**
   * Адрес для получения состояния системы.
   */
  const SYSTEM_STATUS_URL = "StateSystem";
  const SHIFT_URL = 'Shift';
  /**
   * Адррес для передачи комманды на ККТ.
   */
  const COMMAND_URL = "Command";

  private $appId;
  private $secretKey;
  private $baseUrl;
  private $token = FALSE;
  private $salt = FALSE;
  private $params = array();
  private $paramsJson = '';
  private $method = 'POST';
  private $url;
  private $check;
  private $author;
  private $nds = 20;

  public function __construct() {

    // @TODO change to external vars;
    $this->appId = variable_get('bcheck_appid', ''); #'fec6ec17-6feb-4160-a7c3-8a06e036e248';
    $this->secretKey = variable_get('bcheck_secret_key', ''); #'P51lUEHjTwNz4qRfvJ8g9ChVpZAO7LFI';
    $this->baseUrl = 'https://check.business.ru/open-api/v1/';
    $this->author = variable_get('bcheck_cashier', 'тестовый кассир');
    $this->generateSalt();
    $this->retirveToken();
  }

  private function generateSalt() {
    $this->salt = "salt_" . mt_rand(0, PHP_INT_MAX) . str_replace(".", "", microtime(TRUE));
  }

  private function retirveToken() {
    $this->setGet();
    $this->url = self::GET_TOKEN_URL;
    $this->prepareParams(FALSE);
    $response = $this->sendRequest();
    try {
      if (!isset($response['token'])) {
        throw new baseException($response['message']);
      }
      $this->token = $response['token'];

    } catch (baseException $e) {
      print $e->getMessage();
    }


  }

  private function setGet() {
    $this->method = 'GET';
  }

  private function prepareParams($addToken = TRUE) {
    $this->defaultParams($addToken);
    ksort($this->params);
    $this->paramsJson = json_encode($this->params, JSON_UNESCAPED_UNICODE);
  }

  private function defaultParams($addToken = TRUE) {
    $this->generateSalt();
    $this->params['app_id'] = $this->appId;
    $this->params['nonce'] = $this->salt;
    if ($addToken) {
      $this->params['token'] = $this->token;
    }
  }

  private function sendRequest() {
    if (!$this->isGet() && !$this->isPost()) {
      throw Exception('Указан некорректный метод запроса ' . $this->method);
      return FALSE;
    }
    try {
      $cURL = curl_init();
      curl_setopt($cURL, CURLOPT_URL, $this->getRequsetUrl());
      curl_setopt($cURL, CURLOPT_CUSTOMREQUEST, $this->method);
      if ($this->isPost()) {
        curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json; charset=utf-8",
          "accept: application/json",
          "Content-Length: " . strlen($this->paramsJson),
          "sign: " . $this->getSign(),
        ));
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $this->paramsJson);
      }
      else {
        curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
          "sign: " . $this->getSign(),
        ));
      }
      curl_setopt_array(
        $cURL,
        [
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_CONNECTTIMEOUT => 5,
        ]
      );
      $response = curl_exec($cURL);
      if (!$response) {
        throw new baseException('Error request results');
      }
    } catch (baseException $e) {
      print $e->getMessage();
    }
    return json_decode($response, TRUE);
  }

  private function isGet() {
    return $this->method == 'GET';
  }

  private function isPost() {
    return $this->method == 'POST';
  }

  private function getRequsetUrl() {
    return $this->baseUrl . $this->url . ($this->isGet() ? '?' . http_build_query($this->params) : '');
  }

  private function getSign() {
    $sign = md5($this->paramsJson . $this->getSecretKey());
    return $sign;
  }

  public function getSecretKey() {
    return $this->secretKey;
  }

  public function openShift() {
    $this->setPost();
    $this->url = self::COMMAND_URL;
    $this->params = array(
      "type" => "openShift",
      "command" => array(
        "report_type" => FALSE,
        "author" => "Test name",
      ),
    );
    $this->prepareParams();
    $response = $this->sendRequest();
    return $response;
  }

  private function setPost() {
    $this->method = 'POST';
  }

  public function closeShift() {
    $this->setPost();
    $this->url = self::COMMAND_URL;
    $this->params = array(
      "type" => "closeShift",
      "command" => array(
        "report_type" => FALSE,
        "author" => "Test name",
      ),
    );
    $this->prepareParams();
    $response = $this->sendRequest();
    return $response;
  }

  public function getState() {
    $this->setGet();
    $this->url = self::SYSTEM_STATUS_URL;
    $this->prepareParams();
    $response = $this->sendRequest();
    return $response;
  }

  public function shiftIsOpen() {
    $date = new DateTime('now');
    $now = $date->format('d.m.YTH:i:s+03');
    $this->setGet();
    $this->url = self::SHIFT_URL;
    $this->params = array(
      'filter_date_stop_from' => $now,
    );
    $this->prepareParams();
    $response = $this->sendRequest();
    return !empty($response['data']);
  }

  public function addGood($name, $count, $price, $hasNds = TRUE) {
    if (empty($this->check)) {
      $this->createCheck();
    }
    $this->check['goods'][] = array(
      'count' => $count,
      'price' => $price,
      'sum' => $price * $count,
      'name' => $name,
      'nds_value' => $this->nds,
      'nds_not_apply' => !$hasNds,
    );
  }

  private function createCheck() {
    $this->prepareCheck();
  }printBi

  private function prepareCheck() {
    $this->check = array(
      'author' => $this->author,
      'goods' => array(),
    );
  }

  public function printBill($print_params) {
    try {
      if (empty($this->check)) {
        throw new baseException('Check is not exist');
      }
      $payed_cash = isset($print_params['payed_cash'])? $print_params['payed_cash'] : FALSE;
      $cnum = $print_params['cnum'];
      $phone = $print_params['phone'];
      $this->prepareCheckToPrint($cnum, $phone, $payed_cash, $print_params['advanced']);
      $this->params['command'] = $this->check;
      $this->params['type'] = 'printCheck';
      $this->prepareParams();
      $this->setPost();
      $this->url = self::COMMAND_URL;
      $response = $this->sendRequest();
      if (empty($response['command_id'])) {
        throw new baseException($response['message']);
      }
    } catch (baseException $e) {
      print $e->getMessage();
    }
    return $this->params['command'];
  }

  private function prepareCheckToPrint($cnum, $phone, $payed_cash = FALSE, $advanced = array()) {
    $this->check['c_num'] = $cnum;
    $this->check['smsEmail54FZ'] = $this->sanitizePhone($phone);
    $this->check['payed_cashless'] = $payed_cash ? 0 : $this->getCheckSumm();
    $this->check['payed_cash'] = $payed_cash ? $this->getCheckSumm() : 0;
    foreach($advanced as $key => $value){
      $this->check['key'] = $value;
    }
  }

  private function sanitizePhone($phone) {
    $phone = preg_replace('|\D|', '', $phone);
    $first_letter = substr($phone, 0, 1);
    if ($first_letter == 7) {
      $phone = '+' . $phone;
    }
    elseif ($first_letter == 8) {
      $phone = '+7' . substr($phone, 1);
    }
    return $phone;
  }

  private function getCheckSumm() {
    $summ = 0;
    foreach ($this->check['goods'] as $good) {
      $summ += $good['sum'];
    }
    return $summ;
  }

  public function printRefundBill($check) {
    $this->params['command'] = $check;
    $this->params['type'] = 'printPurchaseReturn';
    $this->prepareParams();
    $this->setPost();
    $this->url = self::COMMAND_URL;
    $this->sendRequest();
  }
}

